import React, { useEffect, useState } from 'react';
import './index.css';

function HatList() {
  const [hats, setHats] = useState([]);
  const [editingHat, setEditingHat] = useState(null);

  useEffect(() => {
    fetch('http://localhost:8090/api/hats/')
      .then(response => response.json())
      .then(data => setHats(data.hats));
  }, []);

  const handleDelete = async (hatId) => {
    if (window.confirm('Are you sure you want to delete this hat?')) {
      const response = await fetch(`http://localhost:8090/api/hats/delete/${hatId}/`, { 
        method: 'DELETE',
      });
      if (response.ok) {
        setHats(hats.filter(hat => hat.id !== hatId));
      } else {
        console.error(`Failed to delete hat with ID ${hatId}, status code: ${response.status}`);
      }
    }
  };

  const handleEdit = (hat) => {
    setEditingHat(hat);
  };

  const handleSave = async () => {
    const response = await fetch(`http://localhost:8090/api/hats/update/${editingHat.id}/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(editingHat)
    });

    if (response.ok) {
      setHats(hats.map(hat => hat.id === editingHat.id ? editingHat : hat));
      setEditingHat(null);
    } else {
      console.error(`Failed to update hat with ID ${editingHat.id}, status code: ${response.status}`);
    }
  };


  return (
    <div>
      <h2>Hat List</h2>
      {hats.map(hat => (
        <div key={hat.id} className="hat-item">
          <div className="hat-info">
            {editingHat && editingHat.id === hat.id ? (
              <div className="editing-form">
                <input 
                  type="text"
                  value={editingHat.style_name}
                  onChange={(e) => setEditingHat({...editingHat, style_name: e.target.value})}
                />
                <input 
                  type="text"
                  value={editingHat.color}
                  onChange={(e) => setEditingHat({...editingHat, color: e.target.value})}
                />
                <input 
                  type="text"
                  value={editingHat.fabric}
                  onChange={(e) => setEditingHat({...editingHat, fabric: e.target.value})}
                />
                <input 
                  type="text"
                  value={editingHat.location_id}
                  onChange={(e) => setEditingHat({...editingHat, location_id: e.target.value})}
                />
                <input 
                  type="text"
                  value={editingHat.picture_url}
                  onChange={(e) => setEditingHat({...editingHat, picture_url: e.target.value})}
                />
                <button onClick={handleSave}>Save</button>
              </div>
            ) : (
              <>
                <img src={hat.picture_url} width="100" />
                <span className="hat-name">{hat.style_name}</span>
                <span className="hat-detail">Color: {hat.color}</span>
                <span className="hat-detail">Fabric: {hat.fabric}</span>
                <span className="hat-detail">location_id: {hat.location_id}</span>
              </>
            )}
          </div>
          <button
            onClick={() => handleEdit(hat)}
            className="btn btn-primary hat-button"
          >
            Edit
          </button>
          <button 
            onClick={() => handleDelete(hat.id)}
            className="btn btn-danger hat-button"
          >
            Delete
          </button>
        </div>
      ))}
    </div>
  );
}

export default HatList;
