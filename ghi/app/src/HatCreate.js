import React, { useState } from 'react';

function HatCreate() {
  const [formData, setFormData] = useState({
    fabric: '',
    style_name: '',
    color: '',
    picture_url: '',
    location_id: ''
  });

  const handleCreate = async (event) => {
    event.preventDefault();
    const response = await fetch('http://localhost:8090/api/hats/create/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    });

    if (response.ok) {
      const data = await response.json();
      alert('Hat created with id ' + data.hat.id);
    } else {
      alert('Error creating hat');
    }
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setFormData(prevState => ({ ...prevState, [name]: value }));
  };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h2>Create Hat</h2>
          <form id="create-hat-form" onSubmit={handleCreate}>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="fabric"
                name="fabric"
                placeholder="Fabric"
                value={formData.fabric}
                onChange={handleChange}
              />
              <label htmlFor="fabric">Fabric</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="style_name"
                name="style_name"
                placeholder="Style Name"
                value={formData.style_name}
                onChange={handleChange}
              />
              <label htmlFor="style_name">Style Name</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="color"
                name="color"
                placeholder="color"
                value={formData.color}
                onChange={handleChange}
              />
              <label htmlFor="color">color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="picture_url"
                name="picture_url"
                placeholder="picture_url"
                value={formData.picture_url}
                onChange={handleChange}
              />
              <label htmlFor="picture_url">picture_url</label>
            </div>
            <div className="form-floating mb-3">
              <input
                type="text"
                className="form-control"
                id="location_id"
                name="location_id"
                placeholder="location_id"
                value={formData.location_id}
                onChange={handleChange}
              />
              <label htmlFor="location_id">location_id</label>
            </div>
            <button type="submit" className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default HatCreate;
