import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

from hats_rest.models import LocationVO


WARDROBE_API_ENDPOINT = "http://wardrobe-api:8000/api/locations/"


def get_locations():
    response = requests.get(WARDROBE_API_ENDPOINT)
    content = json.loads(response.content)
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
        )


def poll():
    while True:
        try:
            get_locations()
        except Exception as e:
            print(e)
        time.sleep(5)


if __name__ == "__main__":
    poll()
