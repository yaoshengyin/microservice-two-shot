from django.urls import path
from . import views

urlpatterns = [
    path('hats/', views.list_hats, name='list_hats'),
    path('hats/create/', views.create_hat, name='create_hat'),
    path('hats/delete/<int:hat_id>/', views.delete_hat, name='delete_hat'),
    path('hats/update/<int:hat_id>/', views.update_hat, name='update_hat'),
]
