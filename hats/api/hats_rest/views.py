import json
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from .models import Hat, LocationVO
from django.core.exceptions import ValidationError


@require_http_methods(["GET"])
def list_hats(request):
    hats = Hat.objects.all().values()
    hat_list = list(hats)
    return JsonResponse({'hats': hat_list})


@require_http_methods(["POST"])
def create_hat(request):
    try:
        content = json.loads(request.body.decode('utf-8'))

        try:
            location_id = content["location_id"]
            location = LocationVO.objects.get(pk=location_id)
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse({"status": "Invalid location id"}, status=400)

        del content["location_id"]

        hat = Hat.objects.create(**content)

        return JsonResponse({'hat': {"id": hat.id}})
    except ValidationError as e:
        return JsonResponse({'status': 'Invalid data', 'errors': str(e)}, status=400)
    except json.JSONDecodeError:
        return HttpResponseBadRequest("Invalid JSON")


@require_http_methods(["DELETE"])
def delete_hat(request, hat_id):
    try:
        hat = Hat.objects.get(id=hat_id)
        hat.delete()
        return JsonResponse({'status': 'Deleted'})
    except Hat.DoesNotExist:
        return JsonResponse({'status': 'Not Found'}, status=404)


@require_http_methods(["PUT"])
def update_hat(request, hat_id):
    try:
        hat = Hat.objects.get(id=hat_id)
    except Hat.DoesNotExist:
        return JsonResponse({'status': 'Not Found'}, status=404)

    try:
        data = json.loads(request.body.decode('utf-8'))
        if 'location_id' in data:
            location = LocationVO.objects.get(pk=data['location_id'])
            data['location'] = location
        for field in ['fabric', 'style_name', 'color', 'picture_url', 'location']:
            if field in data:
                setattr(hat, field, data[field])
        hat.full_clean()
        hat.save()
        return JsonResponse({'status': 'Updated'})
    except ValidationError as e:
        return JsonResponse({'status': 'Invalid data', 'errors': str(e)}, status=400)
    except json.JSONDecodeError:
        return HttpResponseBadRequest("Invalid JSON")
