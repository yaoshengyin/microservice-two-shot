# Wardrobify

Team:

* Yaosheng Yin - The Hats microservices
* Yaosheng Yin - The Shoes microservices

## Design

### The project is Wardrobify, an application that allows people with large and many closets to organize their hats and shoes.
### The starter application comes with a fully-functioning scaffold of microservices, a front-end application, and a database. The bold text services in the following list are the ones that you and your teammate will implement:

* Wardrobe API: provides Location and Bin RESTful API endpoints
* Database: the PostgreSQL database that will hold the data of all of the microservices
* React: the React-based front-end application where both you and your teammate will write the components to interact with your services
* Shoes API: the RESTful API to interact with Shoe resources
* Shoes Poller: the poller to poll the Wardrobe API for Bin resources
* Hats API: the RESTful API to interact with Hat resources
* Hats Poller: the poller to poll the Wardrobe API for Location resources


## Shoes microservice

Design & Models
The Shoes microservice is a Django-based API responsible for managing all information related to shoes.
### The Shoe model consists of the following fields:
* 'manufacturer': The name of the shoe manufacturer (String)
* 'model_name': The name of the specific shoe model (String)
* 'color': The color of the shoe (String)
* 'picture_url': A URL pointing to a picture of the shoe (String)
* 'bin_id': Foreign Key relation to the Bin model in the Wardrobe API, identifying where the shoe is stored (Integer)

#### Integration with Wardrobe Microservice
The Shoes microservice integrates with the Wardrobe API through its poller application (shoes/poll). This poller frequently requests the GET /api/bins/ and GET /api/bins/<int:pk>/ endpoints to update its list of bins.



## Hats microservice

Design & Models
The Hats microservice is also a Django-based API and handles all data and operations related to hats. 
### The Hat model has the following fields:
* 'fabric': The fabric the hat is made from (String)
* 'style_name': The style or type of the hat (String)
* 'color': The color of the hat (String)
* 'picture_url': A URL pointing to a picture of the hat (String)
* 'location_id': Foreign Key relation to the Location model in the Wardrobe API, describing where the hat is stored (Integer)

#### Integration with Wardrobe Microservice
The Hats microservice interacts with the Wardrobe API via its poller application (hats/poll). This poller routinely fetches data from the GET /api/locations/ and GET /api/locations/<int:pk>/ endpoints to keep its list of locations up-to-date.
It has full RESTful APIs to interact with Location resources.
